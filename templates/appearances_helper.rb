module AppearancesHelper
  def brand_item
    true
  end

  def brand_title
    'Kwant Gitlab'
  end

  def brand_image
    image_tag 'kwant_full_logo.png'
  end

  def brand_text
    'This is the Kwant Gitlab'
  end

  def brand_header_logo
    image_tag 'kwant_logo.png'
  end
end
